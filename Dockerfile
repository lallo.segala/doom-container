
FROM ubuntu:22.04 AS prod

RUN apt-get update -y && apt-get install -y \
    crispy-doom

WORKDIR /data

CMD /usr/games/crispy-doom
