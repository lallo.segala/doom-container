xhost + local:
docker run \
    -it \
    --rm \
    --net=host \
    --env="DISPLAY" \
    --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
    --volume="$HOME/doom-container/saves/:/root/.local/share/crispy-doom/" \
    --volume="$HOME/doom-container/data/:/data:ro" \
    --privileged \
    registry.gitlab.com/lallo.segala/doom-container $@
