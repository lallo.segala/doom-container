# doom-container

To play run:
```
xhost + local:
docker run \
    -it \
    --net=host \
    --env="DISPLAY" \
    --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
    registry.gitlab.com/lallo.segala/doom-container
```

To add sounds add the ```--privileged``` flag in the run options
